public class Protein implements Comparable {
	private String identifier;
	private String sequence;
	private int counter;
	private int match;

	public Protein(String i, String s, int c){
		identifier = i;
		sequence = s;
		counter = c;
		match = 0;
	}
	public String getIdentifier(){
		return identifier;
	}
	public String getSequence(){
	return sequence;
	}
	public int getCounter(){
	return counter;
	}
	public void setCounter(int value){
	counter = value;
	}
	public void setMatch(int value){
	match = value;
	}
	public int getMatch(){
		return match;
	}
	public int compareTo(Object object){
		return Integer.compare(counter, counter);
	}

}

	
