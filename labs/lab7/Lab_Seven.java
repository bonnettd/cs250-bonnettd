//********************************
// Dan Bonnett
// CMPSCI 250 Lab 7
// Sorting Strings
// Honor Code: This work is my own unless otherwise cited.
// *******************************

import java.util.*;
import java.io.*;

public class Lab_Seven {
    static String [] identifier;
    static String [] sequence;
    static int [] count;
    static int min = 0;
    static int max = 0;
    //static int aminos = 0;

    public static void main(String args[]) throws IOException {

        int avg = 0;
        String [] a = StdIn.readAllStrings();
        int size = a.length/2;
        String [] identifier = new String[a.length/2];

        for(int k = 0; k < a.length; k += 2) {
            identifier[k/2] = a[k];
        } //for

        String [] sequence = new String[a.length/2];

        for(int j = 1; j < a.length; j += 2) {
            sequence[j/2] = a[j];
        } //for

        int [] count = new int[size];

        for(int c = 0; c < sequence.length; c++){
            String s = sequence[c];
            int counter = 0;
            for(int b = 0; b < s.length(); b++){
                if(s.charAt(b) == '-') {
                    counter++;
                }
            }
            count[c] = counter;
        }

        Protein[] proteins = new Protein[size];
        fill(proteins,count);
        int matches = 0;
        int min = proteins[0].getCounter();
        int minIndex = 0;
        for(int k = 0; k < proteins.length; k++) {
            if (min > proteins[k].getCounter()) {
                min = proteins[k].getCounter();
                minIndex = k;
            } //if
        } //for

        int max = proteins[0].getCounter();
        int maxIndex = 0;
        for(int l = 0; l < proteins.length; l++) {
            if(max < proteins[l].getCounter()) {
                max = proteins[l].getCounter();
                maxIndex = l;
            } //if
        } //for

        System.out.println(" ");
        System.out.println("*****PART 1*****\n");
        System.out.println("Most complete identifier is " + proteins[minIndex].getIdentifier() + " with " + min + " missing \n");
        System.out.println("Most incomplete identifier is " + proteins[maxIndex].getIdentifier() + " with " + max + " missing \n");



        String dTim = proteins[639].getSequence();

        match(proteins, dTim);

        int minMatchIndex = 0;
        int minMatch = proteins[0].getMatch();
        for(int k = 0; k < proteins.length; k++) {
            if (minMatch > proteins[k].getMatch()) {
                minMatch = proteins[k].getMatch();
                minMatchIndex = k;
            }
        }

        int maxMatchIndex = 0;
        int maxMatch = proteins[0].getMatch();
        for(int l = 0; l < proteins.length; l++) {
            if(maxMatch < proteins[l].getMatch()) {
                maxMatch = proteins[l].getMatch();
                maxMatchIndex = l;
            }
        }

        System.out.println("*****PART 2*****\n");
        System.out.println("Least similar identifier is " + proteins[minMatchIndex].getIdentifier() + " with " + minMatch +" matches\n");
        System.out.println("Most similar identifier is " + proteins[maxMatchIndex].getIdentifier() + " with " + maxMatch + " matches\n");

        for(int k = 0; k < proteins.length; k++){
            matches += proteins[k].getMatch();
        }

        System.out.println("Average number of matches is:  "+ matches/640 + "\n");

        String alg = "MSD";
        String alg2 = "LSD";
        CompareTiming(alg, alg2, identifier, sequence);
        Arrays.sort(proteins);

    } //main

    public static void fill(Protein[] proteins, int [] count) throws IOException {
        Scanner scan = null;
        scan = new Scanner(new BufferedReader(new FileReader("cTIM_core_align.fa")));
        int n = 0;
        while (scan.hasNext()){
            String i = scan.next();
            String s = scan.next();
            proteins[n] = new Protein(i,s,count[n]);
            n++;
        } //while

    } //fill

    public static void match(Protein [] protein, String dTim) {
        for (int i = 0; i < dTim.length(); i++) {
            char x = dTim.charAt(i);
            for(int k = 0; k < protein.length-1; k++) {
                char y = protein[k].getSequence().charAt(i);
                if(x == y) {
                    protein[k].setMatch(protein[k].getMatch() + 3);
                }
                else if (y == '-') {
                    protein[k].setMatch(protein[k].getMatch() - 1);
                }
                else if (x != y) {
                    protein[k].setMatch(protein[k].getMatch() - 2);
                }
            }
        }
    } //match


    public static double getTime(String alg, String[] a, int t) {
        Stopwatch timer = new Stopwatch();
        double time = 0;
        if (alg.equals("Merge")) {
            Merge.sort(a);
        }
        else if (alg.equals("Quick")) {
            Quick.sort(a);
        }
        else if (alg.equals("LSD")) {
            LSD.sort(a, t);
        }
        else if(alg.equals("MSD")) {
            MSD.sort(a);
        }

        time = timer.elapsedTime() + time;
        return timer.elapsedTime();
    } //getTime


    public static void CompareTiming(String alg1, String alg2, String [] identifier, String [] sequence) {
        int length = LengthID(identifier);
        for (int j = 0; j < identifier.length; j++) {
            if (identifier[j].length() < length) {
                int z = identifier[j].length();
                for(int k = 0; k < (length - z); k++) {
                    identifier[j] = identifier[j] + " ";
                } //for
            } //if
        } //for

        System.out.println("*****PART 3*****\n");

        for (int t = 0; t < 6; t++) {

            double id1 = getTime(alg1, identifier, 0);
            double id2 = getTime(alg2, identifier, 0);
            double seq1 = getTime(alg1, sequence, 0);
            double seq2 = getTime(alg2, sequence, 0);

            System.out.println("Identifier: " + alg2 + " is " + (id2/id1) + " times faster than " + alg1);
            System.out.println("Sequence: " + alg2 + " is " + (seq2/seq1) + " times faster than " + alg1 + "\n");
        }

    }

    public static int LengthID(String [] identifier) {
        int len = identifier[0].length();
        for(int i = 0; i < identifier.length; i++){
            if(identifier[i].length() > len) {
                len = identifier[i].length();
            }
        }
        return len;
    } // LengthID

} //class


