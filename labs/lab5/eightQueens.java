/**
 * Dan Bonnett
 * CMPSCI 250 Lab 5
 * Solving the Eight Queens problem (1's represent queens in output)
 * Honor Code: This work is my own unless otherwise cited.
 */

import java.util.*;
import java.io.*;

public class eightQueens {
   static int rowSize;  //number of rows given by the user
   static int colSize;  //number of columns given by the user
   static int solutionCounter = 0;  //variable to count the number of possible solutions
   static int part;  //variable to determine which part of the lab will be executed

   static boolean creating = true; //boolean variable set to true by default
   static boolean[][] board;    //2-D boolean array that will represent our chessboard

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //asks the user which part of the lab to execute:
        System.out.println("Which part of the lab do you wish to run? 1 or 2");

        while (creating) {
            part = sc.nextInt();
            if (part == 1) {
                System.out.println(" "); //printing a line
                creating = false;
                rowSize = 8;
                colSize = 8;
                board = new boolean[8][8];
            } //if
            else if (part == 2) {
                System.out.println("Enter the number of rows and columns of the board");
                rowSize = sc.nextInt();
                colSize = rowSize;
                board = new boolean[rowSize][colSize];
                creating = false;
                System.out.println(" ");
            } //else if
            else {
                //IF THE USER DOES NOT ENTER A 1 OR A 2:
                System.out.println("Entered wrong input. Please enter 1 or 2");
            }
        } //while

        //PRINT A QUEEN IN THE LEFTMOST COLUMN OF THE FIRST ROW
        placeQueen(0);
        //PRINT THE TOTAL NUMBER OF SOLUTIONS FOR THE GIVEN CHESSBOARD SIZE
        System.out.println("Total possible solutions: " + solutionCounter);

    } //main

    /**
     * placeQueen function:
     * Begin the recursive, backtracking exhaustive search
     * Cycle across a row, trying to place a queen in each column, if the placement is legal
     * then a recursive call is made to the next row, if the placement if illegal, then we iterate to the next column
     * and attempt to place a queen there, if we reach the end of a row without finding a valid placement, we backtrack into the previous
     * recursive call
     */

    public static void placeQueen(int row) {
        for (int c = 0; c < colSize; c++) {
            if (isLegalMove(row, c)) {
                addQueen(row, c);
            if (row == rowSize -1) {
                printSolution();
            }
            else {
                placeQueen(row+1);
            } //else
            board[row][c] = false;
            } //if
        } //for
    } //placequeen

    /**
     * isLegalMove function:
     * Determine whether or not a queen can safely be placed at location
     * on the given board
     */

    public static boolean isLegalMove(int row, int col) {
        int x = col;
        int y = col;
        if (row == 0) {
            return true;
        } //if
        else {
            for (int i = row-1; i >= 0; i--) {
                if (board[i][col] == true) {
                    return false;
                }
                if (x > 0) {
                    if (board[i][--x] == true) {
                        return false;
                    }
                }
                if (y < rowSize-1) {
                    if (board[i][++y] == true) {
                        return false;
                    } //if
                } //if
            } //for
        } //else
        return true;
    } //islegalmove


    /**
     * addQueen function:
     */

    public static void addQueen(int row, int col) {
        board[row][col] = true;
    } //addqueen

    /**
     * printSolution function:
     * This function will print out each board with a solution,
     * and will exit after the first solution if the program is
     * executing part 1.
     */

    public static void printSolution() {
        for (int i = 0; i < rowSize; i++) {
            for (int k = 0; k < colSize; k++) {
                if (board[i][k] == true) {
                    System.out.print(" 1 ");
                }
                else {
                    System.out.print(" 0 ");
                }
            }
            System.out.println(" ");
        }
        if (part == 2) {
        System.out.println("##################################");
        }
        if (part == 1) {
            System.exit(0);
        }
            solutionCounter++;

    } //printsolution

} //eightq class
