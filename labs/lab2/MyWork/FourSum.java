import java.io.*;
import java.util.*;

public class FourSum {

    public static void main(String args[]) {
  In in = new In(args[0]);
  int[] a = in.readAllInts();

  Stopwatch timer = new Stopwatch();
  StdOut.println("Quadruples: " + count(a));
  StdOut.println("Elapsed time: " + timer.elapsedTime());
    } //main

 public static int count(int[] a) {

  int count = 0;

  for (int i = 0; i < a.length; i++) {
    System.out.println(i);
   for (int j = i+1; j < a.length; j++) {
    for (int k = j+1; k < a.length; k++) {
        for (int f = k+1; f < a.length; f++) {
     if (a[i] + a[j] + a[k] + a[f] == 0) {
      count++;
     } //if
    } //for
   } //for
  } //for
 } //for

  return count;

 } //count

} //FourSum (class)
